Zing is a locally owned professional lawn and tree care company that knows the needs of Treasure Valley landscapes. We provide honest, professional, clean and courteous service using only the highest quality products available. Our professional staff works hard to provide a superior service that is a great value for our clients.

Address: PO Box 580, Middleton, ID 83644

Phone: 208-585-9400